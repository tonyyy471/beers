class UserHandler {
    static #instance;
    #loginPath = "http://localhost:8080/api/users/authenticate";
    #registerPath = "http://localhost:8080/api/users/register";
    #updatePath = "http://localhost:8080/api/users/update";
    #getLoggedUserPath = "http://localhost:8080/api/users";

    constructor() {
    }

    static getInstance() {
        if (this.#instance === undefined) {
            this.#instance = new UserHandler();
        }
        return this.#instance;
    }

    registerUser($scope, $http, username, password, firstName, lastName, avatar) {
        let register = {
            firstName: firstName,
            lastName: lastName,
            username: username,
            password: password
        };

        $http.post(this.#registerPath, register
        ).then(function () {
            UserHandler.getInstance().login($scope, $http, username, password, avatar);
        }, function (response) {
            $scope.errorMessage = response.data.message;
            $scope.showErrorMessage = true;
        })
    }

    updateUser($scope, $http, password, firstName, lastName, avatar) {
        let update = {
            firstName: firstName,
            lastName: lastName,
            password: password
        };

        $http.post(this.#updatePath, update, {
            headers: {
                "Authorization": "Bearer " + LocalStorageHandler.getJWTToken()
            }
        }).then(function () {
            if (avatar !== null && avatar !== undefined) {
                FileHandler.getInstance().uploadAvatar($scope, $http, avatar);
            } else {
                UserHandler.getInstance().getLoggedUser($scope, $http, LocalStorageHandler.getJWTToken());
            }
        }, function () {
        })
    }

    login($scope, $http, username, password, avatar) {
        let login = {
            username: username,
            password: password
        };

        $http.post(this.#loginPath, login,
        ).then(function (response) {
            LocalStorageHandler.setJWTToken(response.data.token);
            if (avatar !== null && avatar !== undefined) {
                FileHandler.getInstance().uploadAvatar($scope, $http, avatar);
            } else {
                UserHandler.getInstance().getLoggedUser($scope, $http, response.data.token);
            }
        }, function (response) {
            $scope.errorMessage = response.data.message;
            $scope.showErrorMessage = true;
        })
    }

    getLoggedUser($scope, $http, token) {
        let path = "http://localhost:8080/api/users";

        $http.get(this.#getLoggedUserPath, {
            headers: {
                "Authorization": "Bearer " + token
            }
        }).then(function (response) {
            LocalStorageHandler.setLoggedUser(response.data);
            location.reload();
        }, function (response) {
            /*Add error message*/
        });
    }
}