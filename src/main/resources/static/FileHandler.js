class FileHandler {
    static #instance;
    #uploadBeerImagePath = "http://localhost:8080/api/files/upload/beer/";
    #uploadAvatarPath = "http://localhost:8080/api/files/upload/user";

    constructor() {
    }

    static getInstance() {
        if (this.#instance === undefined) {
            this.#instance = new FileHandler();
        }
        return this.#instance;
    }

    uploadAvatar($scope, $http, avatar) {
        let file = new FormData();
        file.append('file', avatar);

        $http.post(this.#uploadAvatarPath, file, {
            headers: {
                'Content-Type': undefined,
                "Authorization": "Bearer " + LocalStorageHandler.getJWTToken()
            }
        }).then(function () {
            UserHandler.getInstance().getLoggedUser($scope, $http, LocalStorageHandler.getJWTToken());
        }, function (response) {
        });
    }

    uploadBeerImage($scope, $http, newBeerImage, newBeerName) {
        let file = new FormData();
        file.append('file', newBeerImage);

        $http.post(this.#uploadBeerImagePath + newBeerName, file, {
            headers: {
                'Content-Type': undefined,
                "Authorization": "Bearer " + LocalStorageHandler.getJWTToken()
            }
        }).then(function () {
            location.reload();
        }, function (response) {
        });
    }
}