class BeerHandler {
    static #instance;
    #getBeersPath = "http://localhost:8080/api/beers/all";
    #createBeerPath = "http://localhost:8080/api/beers/create";
    #editBeerPath = "http://localhost:8080/api/beers/update/";
    #rateBeerPath = "http://localhost:8080/api/ratings";
    #tagBeerPath = "http://localhost:8080/api/tags/";

    constructor() {
    }

    static getInstance() {
        if (this.#instance === undefined) {
            this.#instance = new BeerHandler();
        }
        return this.#instance;
    }

    getTopSixBeers($scope, $http) {
        $http.get(this.#getBeersPath, {
            params: {
                sort: "avgRating"
            }
        }).then(function (response) {
            $scope.topSixBeers = response.data.slice(0, 6);
        }, function () {
        });
    }

    getAllCreatedOrRatedBeers($scope, $http, createdOrRated, filterByCountry, filterByStyle, sortingValue) {
        let created,
            rated,
            country,
            style,
            sort,
            ascending;

        if (createdOrRated === "created")
            created = "created";
        if (createdOrRated === "rated")
            rated = "rated";

        country = filterByCountry === undefined || filterByCountry === "undefined" ? null : filterByCountry;
        style = filterByStyle === undefined || filterByStyle === "undefined" ? null : filterByStyle;

        if (sortingValue === undefined || sortingValue === "undefined" || sortingValue === null) {
            sort = null;
            ascending = null;
        } else {
            if (sortingValue.split(" ")[0] === "Rating")
                sort = "avgRating";
            if (sortingValue.split(" ")[0] === "ABV")
                sort = "abv";
            if (sortingValue.split(" ")[1] === "Ascending")
                ascending = true;
        }

        $http.get(this.#getBeersPath, {
            params: {
                createdBeers: created,
                ratedBeers: rated,
                country: country,
                style: style,
                sort: sort,
                ascending: ascending
            }, headers: {
                "Authorization": "Bearer " + LocalStorageHandler.getJWTToken()
            }
        }).then(function (response) {
            $scope.allCreatedOrRatedBeers = response.data;
        }, function () {
        });
    }

    createNewBeer($scope, $http, newBeerName, newBeerBrewery, newBeerCountry, newBeerABV, newBeerStyle, newBeerImage) {
        let newBeer = {
            name: newBeerName,
            country: newBeerCountry,
            brewery: newBeerBrewery,
            abv: newBeerABV,
            style: newBeerStyle
        };

        $http.post(this.#createBeerPath, newBeer, {
            headers: {
                "Authorization": "Bearer " + LocalStorageHandler.getJWTToken()
            }
        }).then(function () {
            if (newBeerImage !== null && newBeerImage !== undefined) {
                FileHandler.getInstance().uploadBeerImage($scope, $http, newBeerImage, newBeerName);
            } else {
                location.reload();
            }
        }, function (response) {
            $scope.errorMessage = response.data.message;
            $scope.showErrorMessage = true;
        })
    }

    editBeer($scope, $http, beerName, editBeerName, editBeerBrewery, editBeerCountry, editBeerABV, editBeerStyle, editBeerImage) {
        let editBeer = {
            name: editBeerName,
            country: editBeerCountry,
            brewery: editBeerBrewery,
            abv: editBeerABV,
            style: editBeerStyle
        };

        $http.put(this.#editBeerPath + beerName, editBeer, {
            headers: {
                "Authorization": "Bearer " + LocalStorageHandler.getJWTToken()
            }
        }).then(function () {
            if (editBeerImage !== null && editBeerImage !== undefined) {
                if (editBeerName !== null && editBeerName !== undefined) {
                    beerName = editBeerName
                }
                FileHandler.getInstance().uploadBeerImage($scope, $http, editBeerImage, beerName);
            } else {
                location.reload();
            }
        }, function () {
        })
    }

    rateBeer($scope, $http, beerName, ratingValue) {
        let rateBeer = {
            rating: ratingValue,
            beerName: beerName
        };

        $http.post(this.#rateBeerPath, rateBeer, {
            headers: {
                "Authorization": "Bearer " + LocalStorageHandler.getJWTToken()
            }
        }).then(function () {
            location.reload();
        }, function () {
        })
    }

    tagBeer($scope, $http, beerName, tag) {
        let tagBeer = {
            tagValue: tag
        };

        $http.post(this.#tagBeerPath + beerName, tagBeer
        ).then(function () {
            location.reload();
        }, function (response) {
            $scope.errorMessage = response.data.message;
            $scope.showErrorMessage = true;
        })
    }
}