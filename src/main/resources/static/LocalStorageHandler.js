class LocalStorageHandler {
    static setJWTToken(token) {
        localStorage.setItem("JWTToken", token);
    }

    static getJWTToken() {
        return localStorage.getItem("JWTToken");
    }

    static setLoggedUser(user) {
        localStorage.setItem("loggedUser", JSON.stringify(user))
    }

    static getLoggedUser() {
        return JSON.parse(localStorage.getItem("loggedUser"));
    }

    static setCreatedOrRatedBeers(value) {
        localStorage.setItem("showCreatedOrRatedBeers", value)
    }

    static getCreatedOrRatedBeers() {
        return localStorage.getItem("showCreatedOrRatedBeers");
    }

    static setCountryFilter(value) {
        localStorage.setItem("country", value);
    }

    static getCountryFilter() {
        return localStorage.getItem("country");
    }

    static setStyleFilter(value) {
        localStorage.setItem("style", value);
    }

    static getStyleFilter() {
        return localStorage.getItem("style");
    }

    static clearFiltersAndSorts() {
        localStorage.removeItem("country");
        localStorage.removeItem("style");
        localStorage.removeItem("sort");
    }

    static setSort(value) {
        localStorage.setItem("sort", value)
    }

    static getSort() {
        return localStorage.getItem("sort");
    }

    static loadIndexPageIfUserNotLogged() {
        if (LocalStorageHandler.getJWTToken() === null) {
            window.location = "index.html";
        }
    }
}