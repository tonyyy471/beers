let indexApp = angular.module("indexApp", []);

/*5.Show beer tags!*/

indexApp.controller("indexController", function ($scope, $http) {
    $scope.loadResources = function () {
        LocalStorageHandler.clearFiltersAndSorts();
        $scope.getNavigationButtons();
        $scope.getTopSixBeers();
    };

    $scope.showTagAndRateBeer = function () {
        return LocalStorageHandler.getJWTToken() !== null;
    };

    $scope.getNavigationButtons = function () {
        $scope.showLoginAndRegister = LocalStorageHandler.getJWTToken() === null;
    };

    $scope.getAllBeers = function () {
        localStorage.removeItem("showCreatedOrRatedBeers");
        window.location = "all-created-or-rated-beers.html";
    };

    $scope.getBeerRating = function (beer) {
        if (beer.averageRating !== null) {
            return beer.averageRating;
        }
        return "no";
    };

    $scope.rateBeer = function (beerName) {
        let ratingValue = document.getElementById('ratingValue ' + beerName).value;
        BeerHandler.getInstance().rateBeer($scope, $http, beerName, ratingValue)
    };

    $scope.getTopSixBeers = function () {
        BeerHandler.getInstance().getTopSixBeers($scope, $http);
    };

    $scope.setBeerNameForTagging = function (beerName) {
        $scope.beerNameForTagging = beerName;
    };

    $scope.addTag = function (beerTag) {
        BeerHandler.getInstance().tagBeer($scope, $http, $scope.beerNameForTagging, beerTag)
    };

    $scope.register = function (username, password, firstName, lastName) {
        let avatar = $("#avatar")[0].files[0];
        UserHandler.getInstance().registerUser($scope, $http, username, password, firstName, lastName, avatar);
    };

    $scope.login = function (username, password) {
        UserHandler.getInstance().login($scope, $http, username, password, null);
    };

    $scope.logOut = function () {
        localStorage.clear();
        location.reload();
    };

    $scope.loadLoggedUserPage = function () {
        window.location = "logged-user.html";
    };

    $scope.hideErrorMessage = function () {
        $scope.showErrorMessage = false;
    }
});
