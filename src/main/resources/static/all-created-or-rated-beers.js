let createdBeersApp = angular.module("createdBeersApp", []);

createdBeersApp.controller("createdBeersController", function ($scope, $http) {
    let createdOrRatedBeers = LocalStorageHandler.getCreatedOrRatedBeers();

    if (createdOrRatedBeers === null || createdOrRatedBeers === undefined) {
        $scope.showEditButton = false;
        $scope.showLoggedUserUsernameOnPageTitle = false;
        $scope.pageTitle = "ALL BEERS";
    }
    if (createdOrRatedBeers === "created") {
        $scope.showEditButton = true;
        $scope.showLoggedUserUsernameOnPageTitle = true;
        $scope.pageTitle = "CREATED BEERS";
    }
    if (createdOrRatedBeers === "rated") {
        $scope.showEditButton = false;
        $scope.showLoggedUserUsernameOnPageTitle = true;
        $scope.pageTitle = "RATED BEERS";
    }

    $scope.loadResources = function () {
        LocalStorageHandler.loadIndexPageIfUserNotLogged();
        $scope.loadAllCreatedOrRatedBeers();
    };

    $scope.loggedUser = LocalStorageHandler.getLoggedUser();

    $scope.loadAllCreatedOrRatedBeers = function () {
        BeerHandler.getInstance()
            .getAllCreatedOrRatedBeers($scope, $http, LocalStorageHandler.getCreatedOrRatedBeers(),
                LocalStorageHandler.getCountryFilter(), LocalStorageHandler.getStyleFilter(), LocalStorageHandler.getSort());
    };

    $scope.filterOrSort = function (filterByCountry, filterByStyle) {
        LocalStorageHandler.setCountryFilter(filterByCountry);
        LocalStorageHandler.setStyleFilter(filterByStyle);
        LocalStorageHandler.setSort(document.getElementById('sortingValue').value);
        location.reload()
    };

    $scope.getBeerRating = function (beer) {
        if (beer.averageRating !== null) {
            return beer.averageRating;
        }
        return "no";
    };

    $scope.selectBeerForEdition = function (beer) {
        $scope.selectedBeerForEdition = beer;
    };

    $scope.editBeer = function (editBeerName, editBeerBrewery, editBeerCountry, editBeerABV, editBeerStyle) {
        let editBeerImage = $("#editBeerImage")[0].files[0];
        BeerHandler.getInstance().editBeer($scope, $http, $scope.selectedBeerForEdition.name,
            editBeerName, editBeerBrewery, editBeerCountry, editBeerABV, editBeerStyle, editBeerImage);

    };

    $scope.rateBeer = function (beerName) {
        let ratingValue = document.getElementById('ratingValue ' + beerName).value;
        BeerHandler.getInstance().rateBeer($scope, $http, beerName, ratingValue)
    };

    $scope.setBeerNameForTagging = function (beerName) {
        $scope.beerNameForTagging = beerName;
    };

    $scope.addTag = function (beerTag) {
        BeerHandler.getInstance().tagBeer($scope, $http, $scope.beerNameForTagging, beerTag)
    };

    $scope.logOut = function () {
        localStorage.clear();
        window.location = "index.html"
    };

    $scope.loadLoggedUserPage = function () {
        window.location = "logged-user.html";
    };

    $scope.hideErrorMessage = function () {
        $scope.showErrorMessage = false;
    }
});