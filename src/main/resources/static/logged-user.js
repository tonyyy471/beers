let loggedUserApp = angular.module("loggedUserApp", []);

loggedUserApp.controller("loggedUserController", function ($scope, $http) {
    $scope.loggedUser = LocalStorageHandler.getLoggedUser();

    $scope.loadResources = function () {
        LocalStorageHandler.clearFiltersAndSorts();
        LocalStorageHandler.loadIndexPageIfUserNotLogged();
    };

    $scope.getCreatedBeers = function () {
        LocalStorageHandler.setCreatedOrRatedBeers("created");
        window.location = "all-created-or-rated-beers.html";
    };

    $scope.getRatedBeers = function () {
        LocalStorageHandler.setCreatedOrRatedBeers("rated");
        window.location = "all-created-or-rated-beers.html";
    };

    $scope.createNewBeer = function (newBeerName, newBeerBrewery,
                                     newBeerCountry, newBeerABV, newBeerStyle) {
        let newBeerImage = $("#newBeerImage")[0].files[0];

        BeerHandler.getInstance().createNewBeer($scope, $http, newBeerName, newBeerBrewery,
            newBeerCountry, newBeerABV, newBeerStyle, newBeerImage);
    };

    $scope.updateUser = function (updatePassword, updateFirstName, updateLastName) {
        let avatar = $("#avatar")[0].files[0];
        UserHandler.getInstance().updateUser($scope, $http, updatePassword, updateFirstName, updateLastName, avatar)
    };

    $scope.logOut = function () {
        localStorage.clear();
        window.location = "index.html"
    };

    $scope.hideErrorMessage = function () {
        $scope.showErrorMessage = false;
    }
});