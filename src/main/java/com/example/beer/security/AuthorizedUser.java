package com.example.beer.security;

import com.example.beer.models.User;
import com.example.beer.repostiories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AuthorizedUser {
    private UserRepository userRepository;
    private JwtTokenProvider jwtTokenProvider;
    private static AuthorizedUser authorizedUser;

    @Autowired
    public AuthorizedUser(UserRepository userRepository, JwtTokenProvider jwtTokenProvider) {
        this.userRepository = userRepository;
        this.jwtTokenProvider = jwtTokenProvider;
        authorizedUser = this;
    }

    public static AuthorizedUser getInstance() {
        return authorizedUser;
    }

    public User getUser(HttpServletRequest httpServletRequest) {
        String token = jwtTokenProvider.resolveToken(httpServletRequest);
        return userRepository.getUserByUsername(jwtTokenProvider.getUsername(token));
    }

    public User getOptionalUser(HttpServletRequest httpServletRequest) {
        User user = null;
        try {
            user = getUser(httpServletRequest);
        } catch (Exception ignored) {
        }
        return user;
    }
}
