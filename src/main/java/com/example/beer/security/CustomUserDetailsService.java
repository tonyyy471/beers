package com.example.beer.security;

import com.example.beer.models.User;
import com.example.beer.models.enums.Role;
import com.example.beer.repostiories.UserRepository;
import com.example.beer.services.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;


@Service
public class CustomUserDetailsService implements UserDetailsService {
    private UserRepository userRepository;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String alias) {
        final User user = userRepository.getUserByUsername(alias);
        UserValidator.getInstance().userIsNullValidation(user, false, alias);

        return org.springframework.security.core.userdetails.User
                .withUsername(alias)
                .password(user.getPassword())
                .authorities(Role.provideRoles(alias))
                .build();
    }
}
