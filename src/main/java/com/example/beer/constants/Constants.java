package com.example.beer.constants;

public class Constants {
    public static final String ALIAS_ALREADY_EXISTS = "User with alias %s already exists!";
    public static final String ILLEGAL_ALIAS_LENGTH = "Alias should be between 5 and 15 symbols!";
    public static final String ILLEGAL_PASSWORD_LENGTH = "Password should be between 5 and 20 symbols!";
    public static final String USER_NOT_FOUND = "User not found!";
    public static final String BEER_ALREADY_EXISTS = "Beer with name %s already exists!";
    public static final String FIELDS_SHOULD_NOT_BE_EMPTY = "Fields should not be empty!";
    public static final String BEER_NOT_FOUND = "Beer not found!";
    public static final String ILLEGAL_RATING_RANGE = "Rating should be between 1 and 10!";
    public static final String ILLEGAL_TAG_VALUE = "Tag should not be empty!";
    public static final String NOT_YOUR_BEER = "Beer is not yours!";
    public static final String ILLEGAL_USERNAME_OR_PASSWORD = "Illegal alias or password!";
    public static final String FILE_UPLOAD_FAILED = "File upload failed!";
    public static final String NO_FILE_SELECTED = "No file selected!";
}
