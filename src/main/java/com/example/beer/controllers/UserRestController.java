package com.example.beer.controllers;

import com.example.beer.models.dtos.*;
import com.example.beer.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("api/users")
public class UserRestController {
    private UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<UserDTO> users(HttpServletRequest httpServletRequest) {
        UserDTO userDTO = userService.getUser(httpServletRequest);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @PostMapping("/register")
    public void users(@RequestBody CreateUserDTO createUserDTO) {
        userService.registerUser(createUserDTO);
    }

    @PostMapping("/update")
    public void users(HttpServletRequest httpServletRequest, @RequestBody UpdateUserDTO updateUserDTO) {
        userService.updateUser(updateUserDTO, httpServletRequest);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTTokenDTO> authenticate(@RequestBody LoginDTO loginDTO) {
        JWTTokenDTO jwtTokenDTO = userService.login(loginDTO);
        return new ResponseEntity<>(jwtTokenDTO, HttpStatus.OK);
    }
}
