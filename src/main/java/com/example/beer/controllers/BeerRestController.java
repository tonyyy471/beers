package com.example.beer.controllers;

import com.example.beer.models.dtos.BeerDTO;
import com.example.beer.models.dtos.CreateBeerDTO;
import com.example.beer.models.dtos.UpdateBeerDTO;
import com.example.beer.services.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("api/beers")
public class BeerRestController {
    private BeerService beerService;

    @Autowired
    public BeerRestController(BeerService beerService) {
        this.beerService = beerService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<BeerDTO>> beers(HttpServletRequest httpServletRequest,
                                               @RequestParam(required = false) String createdBeers,
                                               @RequestParam(required = false) String ratedBeers,
                                               @RequestParam(required = false) String country,
                                               @RequestParam(required = false) String style,
                                               @RequestParam(required = false) List<String> tags,
                                               @RequestParam(required = false) Double minAbv,
                                               @RequestParam(required = false) Double maxAbv,
                                               @RequestParam(required = false) Double minAvgRating,
                                               @RequestParam(required = false) Double maxAvgRating,
                                               @RequestParam(required = false) String sort,
                                               @RequestParam(required = false) Boolean ascending) {
        System.out.println("Country " + country);
        System.out.println("Style " + style);
        System.out.println("Sort " + sort);
        System.out.println("Ascending " + ascending);

        List<BeerDTO> beerDTOS = createdBeers != null ? beerService.getCreatedBeers(httpServletRequest, country, style,
                tags, minAbv, maxAbv, minAvgRating,
                maxAvgRating, sort, ascending) : ratedBeers != null ? beerService.getRatedBeers(httpServletRequest, country, style,
                tags, minAbv, maxAbv, minAvgRating,
                maxAvgRating, sort, ascending) : beerService.getAllBeers(httpServletRequest, country, style,
                tags, minAbv, maxAbv, minAvgRating,
                maxAvgRating, sort, ascending);
        return new ResponseEntity<>(beerDTOS, HttpStatus.OK);
    }

    @GetMapping("/single")
    public ResponseEntity<BeerDTO> beers(HttpServletRequest httpServletRequest,
                                         @RequestParam String beerName) {
        BeerDTO beerDTO = beerService.getBeer(httpServletRequest, beerName);
        return new ResponseEntity<>(beerDTO, HttpStatus.OK);
    }

    @PostMapping("/create")
    public void beers(HttpServletRequest httpServletRequest, @RequestBody CreateBeerDTO createBeerDTO) {
        beerService.createBeer(httpServletRequest, createBeerDTO);
    }

    @PutMapping("/update/{beerName}")
    public void beers(HttpServletRequest httpServletRequest, @PathVariable String beerName, @RequestBody UpdateBeerDTO updateBeerDTO) {
        beerService.updateBeer(httpServletRequest, updateBeerDTO, beerName);
    }
}
