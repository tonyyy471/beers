package com.example.beer.controllers;

import com.example.beer.models.dtos.CreateTagDTO;
import com.example.beer.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("api/tags")
public class TagRestController {
    private TagService tagService;

    @Autowired
    public TagRestController(TagService tagService) {
        this.tagService = tagService;
    }

    @PostMapping("/{beerName}")
    public void tags(@RequestBody CreateTagDTO createTagDTO,
                     @PathVariable String beerName) {
        tagService.createTag(createTagDTO, beerName);
    }
}
