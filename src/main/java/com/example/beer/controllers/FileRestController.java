package com.example.beer.controllers;

import com.example.beer.services.FileService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("api/files/upload")
public class FileRestController {
    private FileService fileService;

    public FileRestController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping("/user")
    public void users(HttpServletRequest httpServletRequest,
                      @RequestParam(name = "file", required = false) MultipartFile multipartFile) {
        fileService.uploadAvatar(httpServletRequest, multipartFile);
    }

    @PostMapping("/beer/{beerName}")
    public void beers(HttpServletRequest httpServletRequest,
                      @RequestParam(name = "file", required = false) MultipartFile multipartFile, @PathVariable String beerName) {
        System.out.println(multipartFile.getName());
        fileService.uploadImage(httpServletRequest, multipartFile, beerName);
    }
}
