package com.example.beer.controllers;

import com.example.beer.models.dtos.CreateRatingDTO;
import com.example.beer.services.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("api/ratings")
public class RatingRestController {
    private RatingService ratingService;

    @Autowired
    public RatingRestController(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @PostMapping
    public void ratings(HttpServletRequest httpServletRequest, @RequestBody CreateRatingDTO createRatingDTO) {
        ratingService.rateBeer(httpServletRequest, createRatingDTO);
    }
}
