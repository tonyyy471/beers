package com.example.beer.repostiories;

import com.example.beer.models.Beer;
import com.example.beer.models.File;
import com.example.beer.models.User;

public interface FileRepository {
    void save(User user, File avatar);

    void save(Beer beer, File picture);

    File getDefaultPicture();
}
