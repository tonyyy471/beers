package com.example.beer.repostiories;

import com.example.beer.models.BeerTag;
import com.example.beer.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {
    private SessionFactory sessionFactory;

    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Tag createTag(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(tag);
            session.getTransaction().commit();
        }
        return tag;
    }

    @Override
    public Tag getTag(String tagValue) {
        Tag tag;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<Tag> query = session.createQuery("from Tag where tagValue = :tagValue", Tag.class);
            query.setParameter("tagValue", tagValue);
            tag = query.stream().findFirst().orElse(null);
            session.getTransaction().commit();
        }
        return tag;
    }

    @Override
    public void createBeerTag(BeerTag beerTag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(beerTag);
            session.getTransaction().commit();
        }
    }

    @Override
    public BeerTag getBeerTag(int tagId, int beerId) {
        BeerTag beerTag;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<BeerTag> query = session.createQuery("from BeerTag " +
                    "where tag.id = :tagId " +
                    "and beerId = :beerId", BeerTag.class);
            query.setParameter("tagId", tagId);
            query.setParameter("beerId", beerId);
            beerTag = query.stream().findFirst().orElse(null);
            session.getTransaction().commit();
        }
        return beerTag;
    }

    @Override
    public List<BeerTag> getTags(int beerId) {
        List<BeerTag> beerTags;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<BeerTag> query = session.createQuery("from BeerTag where beerId = :beerId", BeerTag.class);
            query.setParameter("beerId", beerId);
            beerTags = query.list();


            session.getTransaction().commit();
        }
        return beerTags;
    }
}
