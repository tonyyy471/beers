package com.example.beer.repostiories;

import com.example.beer.models.Beer;
import com.example.beer.models.File;
import com.example.beer.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FileRepositoryImpl implements FileRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public FileRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(User user, File avatar) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            File oldAvatar = user.getAvatar();
            if (oldAvatar != null) {
                session.delete(oldAvatar);
            }
            session.save(avatar);
            user.setAvatar(avatar);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void save(Beer beer, File picture) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            File oldPicture = beer.getPicture();
            if (oldPicture != null) {
                session.delete(oldPicture);
            }
            session.save(picture);
            beer.setPicture(picture);
            session.update(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public File getDefaultPicture() {
        Session session = sessionFactory.getCurrentSession();
        return session.get(File.class, 22);
    }
}
