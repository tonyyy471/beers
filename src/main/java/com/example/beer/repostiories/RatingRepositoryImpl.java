package com.example.beer.repostiories;

import com.example.beer.models.Rating;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RatingRepositoryImpl implements RatingRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Rating getRating(int beerId, int userId) {
        Rating rating;
        try (Session session = sessionFactory.openSession()) {
            Query<Rating> query = session.createQuery("from Rating " +
                    "where beer.id = :beerId " +
                    "and ratingGiverId = :userId", Rating.class);
            query.setParameter("beerId", beerId);
            query.setParameter("userId", userId);
            rating = query.stream().findFirst().orElse(null);
        }
        return rating;
    }

    @Override
    public void updateRating(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(rating);
            session.getTransaction().commit();
        }
    }

    @Override
    public void createRating(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(rating);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Rating> getRatings(int beerId) {
        List<Rating> ratings;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<Rating> query = session.createQuery("from Rating where beer.id = :beerId", Rating.class);
            query.setParameter("beerId", beerId);
            ratings = query.list();
            session.getTransaction().commit();
        }
        return ratings;
    }

    @Override
    public List<Rating> getRatingsForUser(int userId) {
        List<Rating> ratings;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<Rating> query = session.createQuery("from Rating where ratingGiverId = :userId", Rating.class);
            query.setParameter("userId", userId);
            ratings = query.list();
            session.getTransaction().commit();
        }
        return ratings;
    }
}
