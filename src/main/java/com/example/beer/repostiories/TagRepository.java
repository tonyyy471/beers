package com.example.beer.repostiories;

import com.example.beer.models.BeerTag;
import com.example.beer.models.Tag;

import java.util.List;

public interface TagRepository {
    Tag createTag(Tag tag);

    Tag getTag(String tagValue);

    void createBeerTag(BeerTag beerTag);

    BeerTag getBeerTag(int tagId, int beerId);

    List<BeerTag> getTags(int beerId);
}
