package com.example.beer.repostiories;

import com.example.beer.models.User;

public interface UserRepository {
    User getUserByUsername(String username);

    void registerUser(User newUser);

    void updateUser(User updatedUser);
}
