package com.example.beer.repostiories;

import com.example.beer.models.Beer;
import com.example.beer.models.File;
import com.example.beer.models.Rating;

import java.util.List;

public interface BeerRepository {
    List<Beer> getAllBeers();

    List<Beer> getCreatedBeers(int userId);

    Beer getBeer(int id);

    Beer getBeer(String beerName);

    void createBeer(Beer beer);

    void updateBeer(Beer beer);
}
