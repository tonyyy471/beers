package com.example.beer.repostiories;

import com.example.beer.models.File;
import com.example.beer.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User getUserByUsername(String username) {
        User user;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            user = query.getResultStream().findFirst().orElse(null);
        }
        return user;
    }

    @Override
    public void registerUser(User newUser) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(newUser);
            session.getTransaction().commit();
        }
    }

    @Override
    public void updateUser(User updatedUser) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(updatedUser);
            session.getTransaction().commit();
        }
    }
}
