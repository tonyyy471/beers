package com.example.beer.repostiories;

import com.example.beer.models.Rating;

import java.util.List;

public interface RatingRepository {
    Rating getRating(int beerId, int userId);

    void updateRating(Rating rating);

    void createRating(Rating rating);

    List<Rating> getRatings(int beerId);

    List<Rating> getRatingsForUser(int userId);
}
