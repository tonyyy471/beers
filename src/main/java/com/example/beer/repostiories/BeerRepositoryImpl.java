package com.example.beer.repostiories;

import com.example.beer.models.Beer;
import com.example.beer.models.File;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
public class BeerRepositoryImpl implements BeerRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Beer> getAllBeers() {
        List<Beer> beers;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<Beer> query = session.createQuery("from Beer", Beer.class);
            beers = query.list();
            session.getTransaction().commit();
        }
        return beers;
    }

    @Override
    public List<Beer> getCreatedBeers(int userId) {
        List<Beer> beers;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<Beer> query = session.createQuery("from Beer where creatorId = :userId", Beer.class);
            query.setParameter("userId", userId);
            beers = query.list();
            session.getTransaction().commit();
        }
        return beers;
    }

    @Override
    public Beer getBeer(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Beer.class, id);
    }

    @Override
    public Beer getBeer(String beerName) {
        Beer beer;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<Beer> query = session.createQuery("from Beer where name = :beerName", Beer.class);
            query.setParameter("beerName", beerName);
            beer = query.getResultStream().findFirst().orElse(null);
            session.getTransaction().commit();
        }
        return beer;
    }

    @Override
    public void createBeer(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void updateBeer(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(beer);
            session.getTransaction().commit();
        }
    }
}
