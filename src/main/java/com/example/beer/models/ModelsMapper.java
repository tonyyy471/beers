package com.example.beer.models;

import com.example.beer.models.dtos.*;

import java.util.ArrayList;
import java.util.List;

public class ModelsMapper {
    private static ModelsMapper modelsMapper;

    private ModelsMapper() {
    }

    public static ModelsMapper getInstance() {
        if (modelsMapper == null) {
            modelsMapper = new ModelsMapper();
        }
        return modelsMapper;
    }

    public void updateBeer(UpdateBeerDTO updateBeerDTO, Beer beer) {
        beer.setName(updateBeerDTO.getName());
        beer.setCountry(updateBeerDTO.getCountry());
        beer.setBrewery(updateBeerDTO.getBrewery());
        beer.setStyle(updateBeerDTO.getStyle());
        beer.setABV(updateBeerDTO.getAbv());
    }

    public Beer createBeerDTOtoBeer(CreateBeerDTO createBeerDTO, int userId) {
        return new Beer(createBeerDTO.getName(), userId, createBeerDTO.getCountry(),
                createBeerDTO.getBrewery(), createBeerDTO.getAbv(), createBeerDTO.getStyle(), null);
    }

    public User createUserDTOtoUser(CreateUserDTO createUserDTO) {
        return new User(createUserDTO.getFirstName(), createUserDTO.getLastName(), null,
                createUserDTO.getUsername(), createUserDTO.getPassword());
    }

    public void updateUser(UpdateUserDTO updateUserDTO, User user) {
        user.setFirstName(updateUserDTO.getFirstName());
        user.setLastName(updateUserDTO.getLastName());
        user.setPassword(updateUserDTO.getPassword());
    }

    public BeerDTO beerToBeerDTO(Beer beer, Double averageRating, Double currentUserRating,
                                 int numberOfRatings, List<BeerTag> beerTags) {
        List<String> tags = new ArrayList<>();
        for (BeerTag beerTag : beerTags) {
            String tagValue = beerTag.getTag().getTagValue();
            tags.add(tagValue);
        }
        return new BeerDTO(beer.getId(), beer.getCreatorId(), beer.getName(), beer.getCountry(), beer.getBrewery(), beer.getABV(),
                beer.getStyle(), beer.getPicture(), averageRating, currentUserRating, numberOfRatings, tags);
    }

    public UserDTO userToUserDTO(User user) {
        return new UserDTO(user.getId(), user.getFirstName(), user.getLastName(), user.getUsername(), user.getAvatar());
    }
}
