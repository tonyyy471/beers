package com.example.beer.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "beers")
public class Beer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    private String name;
    @NotNull
    private int creatorId;
    @NotNull
    private String country;
    @NotNull
    private String brewery;
    @NotNull
    private double ABV;
    @NotNull
    private String style;
    @OneToOne
    private File picture;

    public Beer() {
    }

    public Beer(@NotNull String name, @NotNull int creatorId, @NotNull String country,
                @NotNull String brewery, @NotNull double ABV, @NotNull String style, File picture) {
        this.name = name;
        this.creatorId = creatorId;
        this.country = country;
        this.brewery = brewery;
        this.ABV = ABV;
        this.style = style;
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }

    public double getABV() {
        return ABV;
    }

    public void setABV(double ABV) {
        this.ABV = ABV;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public File getPicture() {
        return picture;
    }

    public void setPicture(File picture) {
        this.picture = picture;
    }
}
