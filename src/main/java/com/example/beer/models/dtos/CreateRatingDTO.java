package com.example.beer.models.dtos;

public class CreateRatingDTO {
    private double rating;
    private String beerName;

    public CreateRatingDTO() {
    }

    public CreateRatingDTO(double rating, String beerName) {
        this.rating = rating;
        this.beerName = beerName;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getBeerName() {
        return beerName;
    }

    public void setBeerName(String beerName) {
        this.beerName = beerName;
    }
}
