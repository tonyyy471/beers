package com.example.beer.models.dtos;

import com.example.beer.models.File;

import java.text.DecimalFormat;
import java.util.List;

public class BeerDTO {
    private int id;
    private int creatorId;
    private String name;
    private String country;
    private String brewery;
    private double ABV;
    private String style;
    private File picture;
    private Double averageRating;
    private Double currentUserRating;
    private int numberOfRatings;
    private List<String> tags;

    public BeerDTO() {
    }

    public BeerDTO(int id, int creatorId, String name, String country, String brewery, double ABV, String style, File picture,
                   Double averageRating, Double currentUserRating, int numberOfRatings, List<String> tags) {
        this.id = id;
        this.creatorId = creatorId;
        this.name = name;
        this.country = country;
        this.brewery = brewery;
        this.ABV = ABV;
        this.style = style;
        this.picture = picture;
        this.averageRating = averageRating;
        this.currentUserRating = currentUserRating;
        this.numberOfRatings = numberOfRatings;
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }

    public double getABV() {
        return ABV;
    }

    public void setABV(double ABV) {
        this.ABV = ABV;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public File getPicture() {
        return picture;
    }

    public void setPicture(File picture) {
        this.picture = picture;
    }

    public String getAverageRating() {
        if (this.averageRating == null) {
            return null;
        }
        return new DecimalFormat("##.#").format(this.averageRating);
    }

    public Double averageRatingForSorting() {
        return averageRating == null ? 0 : averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public Double getCurrentUserRating() {
        return currentUserRating;
    }

    public void setCurrentUserRating(Double currentUserRating) {
        this.currentUserRating = currentUserRating;
    }

    public int getNumberOfRatings() {
        return numberOfRatings;
    }

    public void setNumberOfRatings(int numberOfRatings) {
        this.numberOfRatings = numberOfRatings;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public String getTagsAsString() {
        String tags = "";

        for (String tag : this.tags) {
            tags += "#" + tag + " ";
        }
        return tags;
    }
}
