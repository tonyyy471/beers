package com.example.beer.models.dtos;

public class CreateBeerDTO {
    private String name;
    private String country;
    private String brewery;
    private double abv;
    private String style;

    public CreateBeerDTO() {
    }

    public CreateBeerDTO(String name, String country, String brewery,
                         double abv, String style) {
        this.name = name;
        this.country = country;
        this.brewery = brewery;
        this.abv = abv;
        this.style = style;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
