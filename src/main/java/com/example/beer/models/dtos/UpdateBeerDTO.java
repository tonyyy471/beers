package com.example.beer.models.dtos;

public class UpdateBeerDTO {
    private String name;
    private String country;
    private String brewery;
    private Double abv;
    private String style;

    public UpdateBeerDTO() {
    }

    public UpdateBeerDTO(String name, String country, String brewery, Double abv, String style) {
        this.name = name;
        this.country = country;
        this.brewery = brewery;
        this.abv = abv;
        this.style = style;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }

    public Double getAbv() {
        return abv;
    }

    public void setAbv(Double abv) {
        this.abv = abv;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
