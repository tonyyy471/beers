package com.example.beer.models.dtos;

public class CreateTagDTO {
    private String tagValue;

    public CreateTagDTO() {
    }

    public CreateTagDTO(String tagValue) {
        this.tagValue = tagValue;
    }

    public String getTagValue() {
        return tagValue;
    }

    public void setTagValue(String tagValue) {
        this.tagValue = tagValue;
    }
}
