package com.example.beer.models.dtos;

public class JWTTokenDTO {
    private String token;

    public JWTTokenDTO() {
    }

    public JWTTokenDTO(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
