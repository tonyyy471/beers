package com.example.beer.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ratings")
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    private double rating;
    @NotNull
    private int ratingGiverId;
    @NotNull
    @ManyToOne
    private Beer beer;

    public Rating() {
    }

    public Rating(@NotNull double rating, @NotNull int ratingGiverId, @NotNull Beer beer) {
        this.rating = rating;
        this.ratingGiverId = ratingGiverId;
        this.beer = beer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getRatingGiverId() {
        return ratingGiverId;
    }

    public void setRatingGiverId(int ratingGiverId) {
        this.ratingGiverId = ratingGiverId;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }
}
