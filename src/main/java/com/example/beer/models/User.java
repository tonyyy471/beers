package com.example.beer.models;

import com.example.beer.constants.Constants;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @OneToOne
    private File avatar;
    @NotNull
    @Size(min = 5, max = 15, message = Constants.ILLEGAL_ALIAS_LENGTH)
    private String username;
    @NotNull
    @Size(min = 5, message = Constants.ILLEGAL_PASSWORD_LENGTH)
    private String password;

    public User() {
    }

    public User(@NotNull String firstName, @NotNull String lastName, File avatar,
                @NotNull @Size(min = 5, max = 15, message = Constants.ILLEGAL_ALIAS_LENGTH) String username,
                @NotNull @Size(min = 5, message = Constants.ILLEGAL_PASSWORD_LENGTH) String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.avatar = avatar;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public File getAvatar() {
        return avatar;
    }

    public void setAvatar(File avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
