package com.example.beer.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "beer_tags")
public class BeerTag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    @ManyToOne
    private Tag tag;
    @NotNull
    private int beerId;

    public BeerTag() {
    }

    public BeerTag(@NotNull Tag tag, @NotNull int beerId) {
        this.tag = tag;
        this.beerId = beerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }
}
