package com.example.beer.services;

import com.example.beer.constants.Constants;
import com.example.beer.models.Beer;
import com.example.beer.models.BeerTag;
import com.example.beer.models.Tag;
import com.example.beer.models.User;
import com.example.beer.models.dtos.CreateTagDTO;
import com.example.beer.repostiories.BeerRepository;
import com.example.beer.repostiories.TagRepository;
import com.example.beer.security.AuthorizedUser;
import com.example.beer.services.exceptions.UnauthorizedException;
import com.example.beer.services.validators.BeerValidator;
import com.example.beer.services.validators.TagValidator;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class TagServiceImpl implements TagService {
    private TagRepository tagRepository;
    private BeerRepository beerRepository;

    public TagServiceImpl(TagRepository tagRepository, BeerRepository beerRepository) {
        this.tagRepository = tagRepository;
        this.beerRepository = beerRepository;
    }

    @Override
    public void createTag(CreateTagDTO createTagDTO, String beerName) {
        String tagValue = createTagDTO.getTagValue();
        TagValidator.getInstance().tagValueIsNullValidation(tagValue);

        Beer beer = beerRepository.getBeer(beerName);
        BeerValidator.getInstance().beerIsNullValidation(beer, false, null);

        Tag tag = tagRepository.getTag(tagValue);
        if (tag == null) {
            tag = tagRepository.createTag(new Tag(tagValue));
            tagRepository.createBeerTag(new BeerTag(tag, beer.getId()));
        } else {
            int tagId = tag.getId();
            BeerTag beerTag = tagRepository.getBeerTag(tagId, beer.getId());
            if (beerTag == null) {
                tagRepository.createBeerTag(new BeerTag(tag, beer.getId()));
            }
        }
    }
}
