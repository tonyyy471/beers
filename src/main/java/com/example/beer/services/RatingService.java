package com.example.beer.services;

import com.example.beer.models.dtos.CreateRatingDTO;

import javax.servlet.http.HttpServletRequest;

public interface RatingService {
    void rateBeer(HttpServletRequest httpServletRequest, CreateRatingDTO createRatingDTO);
}
