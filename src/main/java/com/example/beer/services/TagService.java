package com.example.beer.services;

import com.example.beer.models.dtos.CreateTagDTO;

public interface TagService {
    void createTag(CreateTagDTO createTagDTO, String beerName);
}
