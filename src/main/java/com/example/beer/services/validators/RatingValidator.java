package com.example.beer.services.validators;

import com.example.beer.constants.Constants;
import com.example.beer.services.exceptions.FieldException;

public class RatingValidator {
    private static RatingValidator ratingValidator;

    private RatingValidator() {
    }

    public static RatingValidator getInstance() {
        if (ratingValidator == null) {
            ratingValidator = new RatingValidator();
        }
        return ratingValidator;
    }

    public void ratingIsBetweenOneAndTen(double rating) {
        if (rating < 1 || rating > 10) {
            throw new FieldException(Constants.ILLEGAL_RATING_RANGE);
        }
    }
}
