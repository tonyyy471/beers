package com.example.beer.services.validators;

import com.example.beer.constants.Constants;
import com.example.beer.models.User;
import com.example.beer.services.exceptions.AlreadyExistsException;
import com.example.beer.services.exceptions.FieldException;
import com.example.beer.services.exceptions.NotFoundException;

public class UserValidator {
    private static UserValidator userValidator;

    private UserValidator() {
    }

    public static UserValidator getInstance() {
        if (userValidator == null) {
            userValidator = new UserValidator();
        }
        return userValidator;
    }

    public void lengthValidation(String value, String errorMessage) {
        if (value == null || value.length() < 5 || value.length() > 25) {
            throw new FieldException(errorMessage);
        }
    }

    public void userIsNullValidation(User user, boolean userMustBeNull, String alias) {
        if (userMustBeNull) {
            if (user != null) {
                throw new AlreadyExistsException(String.format(Constants.ALIAS_ALREADY_EXISTS, alias));
            }
        } else {
            if (user == null) {
                throw new NotFoundException(Constants.USER_NOT_FOUND);
            }
        }
    }
}
