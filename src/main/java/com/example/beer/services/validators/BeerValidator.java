package com.example.beer.services.validators;

import com.example.beer.constants.Constants;
import com.example.beer.models.Beer;
import com.example.beer.services.exceptions.AlreadyExistsException;
import com.example.beer.services.exceptions.FieldException;
import com.example.beer.services.exceptions.IllegalActionException;
import com.example.beer.services.exceptions.NotFoundException;

public class BeerValidator {
    private static BeerValidator beerValidator;

    private BeerValidator() {
    }

    public static BeerValidator getInstance() {
        if (beerValidator == null) {
            beerValidator = new BeerValidator();
        }
        return beerValidator;
    }

    public void beerIsNullValidation(Beer beer, boolean beerMustBeNull, String beerName) {
        if (beerMustBeNull) {
            if (beer != null) {
                throw new AlreadyExistsException(String.format(Constants.BEER_ALREADY_EXISTS, beerName));
            }
        } else {
            if (beer == null) {
                throw new NotFoundException(Constants.BEER_NOT_FOUND);
            }
        }
    }

    public void fieldsNotNullValidation(String name, String country, String brewery, String style) {
        if (name == null || country == null || brewery == null || style == null) {
            throw new FieldException(Constants.FIELDS_SHOULD_NOT_BE_EMPTY);
        }
    }

    public void beerCreatorMatchesValidation(int actualId, int expectedId) {
        if (actualId != expectedId) {
            throw new IllegalActionException(Constants.NOT_YOUR_BEER);
        }
    }
}
