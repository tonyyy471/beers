package com.example.beer.services.validators;

import com.example.beer.constants.Constants;
import com.example.beer.services.exceptions.FieldException;

public class TagValidator {
    private static TagValidator tagValidator;

    private TagValidator() {
    }

    public static TagValidator getInstance() {
        if (tagValidator == null) {
            tagValidator = new TagValidator();
        }
        return tagValidator;
    }

    public void tagValueIsNullValidation(String tagValue){
        if (tagValue == null || tagValue.equals("")){
            throw new FieldException(Constants.ILLEGAL_TAG_VALUE);
        }
    }
}
