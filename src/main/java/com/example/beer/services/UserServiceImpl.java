package com.example.beer.services;

import com.example.beer.constants.Constants;
import com.example.beer.models.dtos.*;
import com.example.beer.models.ModelsMapper;
import com.example.beer.models.User;
import com.example.beer.models.enums.Role;
import com.example.beer.repostiories.UserRepository;
import com.example.beer.security.AuthorizedUser;
import com.example.beer.security.JwtTokenProvider;
import com.example.beer.services.exceptions.UnauthorizedException;
import com.example.beer.services.utils.UpdateFieldHandler;
import com.example.beer.services.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private AuthenticationManager authenticationManager;
    private JwtTokenProvider jwtTokenProvider;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, AuthenticationManager authenticationManager,
                           JwtTokenProvider jwtTokenProvider, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDTO getUser(HttpServletRequest httpServletRequest) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);
        return ModelsMapper.getInstance().userToUserDTO(user);
    }

    public void registerUser(CreateUserDTO createUserDTO) {
        String username = createUserDTO.getUsername();
        String password = createUserDTO.getPassword();

        UserValidator.getInstance().lengthValidation(username, Constants.ILLEGAL_ALIAS_LENGTH);
        UserValidator.getInstance().lengthValidation(password, Constants.ILLEGAL_PASSWORD_LENGTH);

        User user = userRepository.getUserByUsername(username);
        UserValidator.getInstance().userIsNullValidation(user, true, username);

        createUserDTO.setPassword(passwordEncoder.encode(password));

        User newUser = ModelsMapper.getInstance().createUserDTOtoUser(createUserDTO);
        userRepository.registerUser(newUser);
    }

    @Override
    public void updateUser(UpdateUserDTO updateUserDTO, HttpServletRequest httpServletRequest) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);
        UpdateFieldHandler.getInstance().handleUserFieldsToUpdate(updateUserDTO, user, passwordEncoder);

        ModelsMapper.getInstance().updateUser(updateUserDTO, user);
        userRepository.updateUser(user);
    }

    @Override
    public JWTTokenDTO login(LoginDTO loginDTO) {
        String username = loginDTO.getUsername();
        String password = loginDTO.getPassword();

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            return jwtTokenProvider.createToken(username, Role.provideRoles(username));
        } catch (AuthenticationException e) {
            throw new UnauthorizedException(Constants.ILLEGAL_USERNAME_OR_PASSWORD);
        }
    }
}
