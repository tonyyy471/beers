package com.example.beer.services;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

public interface FileService {
    void uploadAvatar(HttpServletRequest httpServletRequest, MultipartFile multipartFile);

    void uploadImage(HttpServletRequest httpServletRequest, MultipartFile multipartFile, String beerName);
}
