package com.example.beer.services.utils;

import com.example.beer.constants.Constants;
import com.example.beer.models.File;
import com.example.beer.services.exceptions.FieldException;
import com.example.beer.services.exceptions.FileException;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public class FileProvider {
    private static FileProvider fileProvider;

    private FileProvider() {
    }

    public static FileProvider getInstance() {
        if (fileProvider == null) {
            fileProvider = new FileProvider();
        }
        return fileProvider;
    }

    public File getFile(MultipartFile multipartFile) {
        if (multipartFile != null) {
            try {
                String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
                return new File(fileName, multipartFile.getContentType(), multipartFile.getBytes());
            } catch (IOException e) {
                throw new FileException(Constants.FILE_UPLOAD_FAILED);
            }
        }
        throw new FieldException(Constants.NO_FILE_SELECTED);
    }
}
