package com.example.beer.services.utils;

import com.example.beer.models.dtos.BeerDTO;

import java.util.List;
import java.util.stream.Collectors;

public class BeerFilterer {
    private static BeerFilterer beerFilterer;

    private BeerFilterer() {
    }

    public static BeerFilterer getInstance() {
        if (beerFilterer == null) {
            beerFilterer = new BeerFilterer();
        }
        return beerFilterer;
    }

    public List<BeerDTO> filter(List<BeerDTO> beerDTOS, String country, String style,
                                List<String> tags, Double minAbv, Double maxAbv,
                                Double minAvgRating, Double maxAvgRating) {
        beerDTOS = filterByCountry(beerDTOS, country);
        beerDTOS = filterByStyle(beerDTOS, style);
        beerDTOS = filterByTags(beerDTOS, tags);
        beerDTOS = filterByMinAndMaxAbv(beerDTOS, minAbv, maxAbv);
        beerDTOS = filterByMinAndMaxAverageRating(beerDTOS, minAvgRating, maxAvgRating);
        return beerDTOS;
    }

    private List<BeerDTO> filterByCountry(List<BeerDTO> beerDTOS, String country) {
        if (country != null) {
            beerDTOS = beerDTOS
                    .stream()
                    .filter(beerDTO -> beerDTO.getCountry().equals(country))
                    .collect(Collectors.toList());
        }
        return beerDTOS;
    }

    private List<BeerDTO> filterByStyle(List<BeerDTO> beerDTOS, String style) {
        if (style != null) {
            beerDTOS = beerDTOS
                    .stream()
                    .filter(beerDTO -> beerDTO.getStyle().equals(style))
                    .collect(Collectors.toList());
        }
        return beerDTOS;
    }

    private List<BeerDTO> filterByTags(List<BeerDTO> beerDTOS, List<String> tags) {
        if (tags != null) {
            beerDTOS = beerDTOS
                    .stream()
                    .filter(beerDTO -> beerDTO.getTags().containsAll(tags))
                    .collect(Collectors.toList());
        }
        return beerDTOS;
    }

    private List<BeerDTO> filterByMinAndMaxAbv(List<BeerDTO> beerDTOS, Double minAbv, Double maxAbv) {
        if (minAbv != null && maxAbv != null) {
            beerDTOS = beerDTOS
                    .stream()
                    .filter(beerDTO -> beerDTO.getABV() >= minAbv && beerDTO.getABV() <= maxAbv)
                    .collect(Collectors.toList());
            return beerDTOS;
        }

        if (minAbv != null) {
            beerDTOS = beerDTOS
                    .stream()
                    .filter(beerDTO -> beerDTO.getABV() >= minAbv)
                    .collect(Collectors.toList());
        }

        if (maxAbv != null) {
            beerDTOS = beerDTOS
                    .stream()
                    .filter(beerDTO -> beerDTO.getABV() <= maxAbv)
                    .collect(Collectors.toList());
        }
        return beerDTOS;
    }

    private List<BeerDTO> filterByMinAndMaxAverageRating(List<BeerDTO> beerDTOS,
                                                         Double minAvgRating, Double maxAvgRating) {
        if (minAvgRating != null && maxAvgRating != null) {
            beerDTOS = beerDTOS
                    .stream()
                    .filter(beerDTO -> beerDTO.getAverageRating() != null && beerDTO.averageRatingForSorting() >= minAvgRating &&
                            beerDTO.averageRatingForSorting() <= maxAvgRating)
                    .collect(Collectors.toList());
            return beerDTOS;
        }

        if (minAvgRating != null) {
            beerDTOS = beerDTOS
                    .stream()
                    .filter(beerDTO -> beerDTO.getAverageRating() != null && beerDTO.averageRatingForSorting() >= minAvgRating)
                    .collect(Collectors.toList());
        }

        if (maxAvgRating != null) {
            beerDTOS = beerDTOS
                    .stream()
                    .filter(beerDTO -> beerDTO.getAverageRating() != null && beerDTO.averageRatingForSorting() <= maxAvgRating)
                    .collect(Collectors.toList());
        }
        return beerDTOS;
    }
}
