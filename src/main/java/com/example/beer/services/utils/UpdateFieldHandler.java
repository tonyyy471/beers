package com.example.beer.services.utils;

import com.example.beer.constants.Constants;
import com.example.beer.models.Beer;
import com.example.beer.models.User;
import com.example.beer.models.dtos.UpdateBeerDTO;
import com.example.beer.models.dtos.UpdateUserDTO;
import com.example.beer.services.validators.UserValidator;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UpdateFieldHandler {
    private static UpdateFieldHandler updateFIeldHandler;

    private UpdateFieldHandler() {
    }

    public static UpdateFieldHandler getInstance() {
        if (updateFIeldHandler == null) {
            updateFIeldHandler = new UpdateFieldHandler();
        }
        return updateFIeldHandler;
    }

    public void handleUserFieldsToUpdate(UpdateUserDTO updateUserDTO, User user, PasswordEncoder passwordEncoder) {
        String firstName = updateUserDTO.getFirstName();
        String lastName = updateUserDTO.getLastName();
        String password = updateUserDTO.getPassword();

        if (firstName == null || firstName.equals("")) {
            updateUserDTO.setFirstName(user.getFirstName());
        }

        if (lastName == null || lastName.equals("")) {
            updateUserDTO.setLastName(user.getLastName());
        }

        if (password == null || password.equals("")) {
            updateUserDTO.setPassword(user.getPassword());
        } else {
            UserValidator.getInstance().lengthValidation(password, Constants.ILLEGAL_PASSWORD_LENGTH);
            updateUserDTO.setPassword(passwordEncoder.encode(password));
        }
    }

    public void handleBeerFieldsToUpdate(UpdateBeerDTO updateBeerDTO, Beer beer) {
        if (updateBeerDTO.getName() == null || updateBeerDTO.getName().equals("")) {
            updateBeerDTO.setName(beer.getName());
        }

        if (updateBeerDTO.getCountry() == null || updateBeerDTO.getCountry().equals("")) {
            updateBeerDTO.setCountry(beer.getCountry());
        }

        if (updateBeerDTO.getBrewery() == null || updateBeerDTO.getBrewery().equals("")) {
            updateBeerDTO.setBrewery(beer.getBrewery());
        }

        if (updateBeerDTO.getAbv() == null) {
            updateBeerDTO.setAbv(beer.getABV());
        }

        if (updateBeerDTO.getStyle() == null || updateBeerDTO.getStyle().equals("")) {
            updateBeerDTO.setStyle(beer.getStyle());
        }
    }
}
