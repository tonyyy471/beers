package com.example.beer.services.utils;

import com.example.beer.models.dtos.BeerDTO;

import java.util.Comparator;
import java.util.List;

public class BeerSorter {
    private static BeerSorter beerSorter;

    private BeerSorter() {
    }

    public static BeerSorter getInstance() {
        if (beerSorter == null) {
            beerSorter = new BeerSorter();
        }
        return beerSorter;
    }

    public List<BeerDTO> sort(List<BeerDTO> beerDTOS, String sort, Boolean ascending) {
        if (sort == null) {
            return beerDTOS;
        }
        switch (sort) {
            case "abv":
                return sortByAbv(beerDTOS, ascending);
            case "avgRating":
                return sortByAvgRating(beerDTOS, ascending);
            case "numberOfRatings":
                return sortByNumberOfRatings(beerDTOS, ascending);
        }
        return beerDTOS;
    }

    private List<BeerDTO> sortByAbv(List<BeerDTO> beerDTOS, Boolean ascending) {
        Comparator<BeerDTO> comparator = Comparator.comparingDouble(BeerDTO::getABV).reversed();
        if (ascending != null && ascending) {
            comparator = Comparator.comparingDouble(BeerDTO::getABV);
        }
        beerDTOS.sort(comparator);
        return beerDTOS;
    }

    private List<BeerDTO> sortByAvgRating(List<BeerDTO> beerDTOS, Boolean ascending) {
        Comparator<BeerDTO> comparator = (o1, o2) -> o1.averageRatingForSorting()
                .equals(o2.averageRatingForSorting()) ? 0 :
                o1.averageRatingForSorting() > o2.averageRatingForSorting() ? -1 : 1;

        if (ascending != null && ascending) {
            comparator = (o1, o2) -> o1.averageRatingForSorting()
                    .equals(o2.averageRatingForSorting()) ? 0 :
                    o1.averageRatingForSorting() > o2.averageRatingForSorting() ? 1 : -1;
        }
        beerDTOS.sort(comparator);
        return beerDTOS;
    }

    private List<BeerDTO> sortByNumberOfRatings(List<BeerDTO> beerDTOS, Boolean ascending) {
        Comparator<BeerDTO> comparator = Comparator.comparingInt(BeerDTO::getNumberOfRatings).reversed();
        if (ascending != null && ascending) {
            comparator = Comparator.comparingInt(BeerDTO::getNumberOfRatings);
        }
        beerDTOS.sort(comparator);
        return beerDTOS;
    }
}
