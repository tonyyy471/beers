package com.example.beer.services;

import com.example.beer.models.Beer;
import com.example.beer.models.File;
import com.example.beer.models.User;
import com.example.beer.repostiories.BeerRepository;
import com.example.beer.repostiories.FileRepository;
import com.example.beer.security.AuthorizedUser;
import com.example.beer.services.utils.FileProvider;
import com.example.beer.services.validators.BeerValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@Service
public class FileServiceImpl implements FileService {
    private FileRepository fileRepository;
    private BeerRepository beerRepository;

    @Autowired
    public FileServiceImpl(FileRepository fileRepository, BeerRepository beerRepository) {
        this.fileRepository = fileRepository;
        this.beerRepository = beerRepository;
    }

    @Override
    public void uploadAvatar(HttpServletRequest httpServletRequest, MultipartFile multipartFile) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);

        File avatar = FileProvider.getInstance().getFile(multipartFile);

        fileRepository.save(user, avatar);
    }

    @Override
    public void uploadImage(HttpServletRequest httpServletRequest, MultipartFile multipartFile, String beerName) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);

        File picture = FileProvider.getInstance().getFile(multipartFile);

        Beer beer = beerRepository.getBeer(beerName);
        BeerValidator.getInstance().beerIsNullValidation(beer, false, null);

        BeerValidator.getInstance().beerCreatorMatchesValidation(beer.getCreatorId(), user.getId());

        fileRepository.save(beer, picture);
    }
}
