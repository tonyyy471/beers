package com.example.beer.services;

import com.example.beer.models.*;
import com.example.beer.models.dtos.BeerDTO;
import com.example.beer.models.dtos.CreateBeerDTO;
import com.example.beer.models.dtos.UpdateBeerDTO;
import com.example.beer.repostiories.BeerRepository;
import com.example.beer.repostiories.FileRepository;
import com.example.beer.repostiories.RatingRepository;
import com.example.beer.repostiories.TagRepository;
import com.example.beer.security.AuthorizedUser;
import com.example.beer.services.utils.BeerFilterer;
import com.example.beer.services.utils.BeerSorter;
import com.example.beer.services.utils.UpdateFieldHandler;
import com.example.beer.services.validators.BeerValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class BeerServiceImpl implements BeerService {
    private BeerRepository beerRepository;
    private RatingRepository ratingRepository;
    private TagRepository tagRepository;
    private FileRepository fileRepository;

    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository, RatingRepository ratingRepository,
                           TagRepository tagRepository, FileRepository fileRepository) {
        this.beerRepository = beerRepository;
        this.ratingRepository = ratingRepository;
        this.tagRepository = tagRepository;
        this.fileRepository = fileRepository;
    }

    @Override
    public List<BeerDTO> getAllBeers(HttpServletRequest httpServletRequest, String country, String style,
                                     List<String> tags, Double minAbv, Double maxAbv,
                                     Double minAvgRating, Double maxAvgRating,
                                     String sort, Boolean ascending) {
        User user = AuthorizedUser.getInstance().getOptionalUser(httpServletRequest);

        Integer usedId = user == null ? null : user.getId();

        List<Beer> beers = beerRepository.getAllBeers();

        return mapFilterAndSortBeersToBeerDTOs(beers, usedId, country, style, tags, minAbv, maxAbv,
                minAvgRating, maxAvgRating, sort, ascending);
    }

    @Override
    public List<BeerDTO> getCreatedBeers(HttpServletRequest httpServletRequest, String country, String style,
                                         List<String> tags, Double minAbv, Double maxAbv,
                                         Double minAvgRating, Double maxAvgRating,
                                         String sort, Boolean ascending) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);

        List<Beer> beers = beerRepository.getCreatedBeers(user.getId());

        return mapFilterAndSortBeersToBeerDTOs(beers, user.getId(), country, style, tags, minAbv, maxAbv,
                minAvgRating, maxAvgRating, sort, ascending);
    }

    @Override
    public List<BeerDTO> getRatedBeers(HttpServletRequest httpServletRequest, String country, String style,
                                       List<String> tags, Double minAbv, Double maxAbv,
                                       Double minAvgRating, Double maxAvgRating,
                                       String sort, Boolean ascending) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);

        List<Rating> ratings = ratingRepository.getRatingsForUser(user.getId());

        List<Beer> beers = new ArrayList<>();
        for (Rating rating : ratings) {
            beers.add(rating.getBeer());
        }

        return mapFilterAndSortBeersToBeerDTOs(beers, user.getId(), country, style, tags, minAbv, maxAbv,
                minAvgRating, maxAvgRating, sort, ascending);
    }

    @Override
    public BeerDTO getBeer(HttpServletRequest httpServletRequest, String beerName) {
        User user = AuthorizedUser.getInstance().getOptionalUser(httpServletRequest);
        Integer usedId = user == null ? null : user.getId();

        Beer beer = beerRepository.getBeer(beerName);
        BeerValidator.getInstance().beerIsNullValidation(beer, false, null);

        return beerToBeerDTO(beer, usedId);
    }

    @Override
    public void createBeer(HttpServletRequest httpServletRequest, CreateBeerDTO createBeerDTO) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);

        String beerName = createBeerDTO.getName();

        BeerValidator.getInstance().fieldsNotNullValidation(createBeerDTO.getName(), createBeerDTO.getCountry(),
                createBeerDTO.getBrewery(), createBeerDTO.getStyle());

        Beer beer = beerRepository.getBeer(beerName);
        BeerValidator.getInstance().beerIsNullValidation(beer, true, beerName);

        Beer newBeer = ModelsMapper.getInstance().createBeerDTOtoBeer(createBeerDTO, user.getId());
        beerRepository.createBeer(newBeer);
    }

    @Override
    public void updateBeer(HttpServletRequest httpServletRequest, UpdateBeerDTO updateBeerDTO, String beerName) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);

        Beer beer = beerRepository.getBeer(beerName);
        BeerValidator.getInstance().beerIsNullValidation(beer, false, null);

        BeerValidator.getInstance().beerCreatorMatchesValidation(beer.getCreatorId(), user.getId());

        UpdateFieldHandler.getInstance().handleBeerFieldsToUpdate(updateBeerDTO, beer);

        if (!beer.getName().equals(updateBeerDTO.getName())) {
            Beer beerWithNewName = beerRepository.getBeer(updateBeerDTO.getName());
            BeerValidator.getInstance().beerIsNullValidation(beerWithNewName, true, updateBeerDTO.getName());
        }

        ModelsMapper.getInstance().updateBeer(updateBeerDTO, beer);
        beerRepository.updateBeer(beer);
    }

    private BeerDTO beerToBeerDTO(Beer beer, Integer userId) {
        List<Rating> ratings = ratingRepository.getRatings(beer.getId());
        Double averageRating = calculateBeerAverageRating(ratings);
        int numberOfRatings = ratings.size();
        List<BeerTag> beerTags = tagRepository.getTags(beer.getId());
        Rating rating = userId == null ? null : ratingRepository.getRating(beer.getId(), userId);
        Double currentUserRating = rating == null ? null : rating.getRating();

        if (beer.getPicture() == null) {
            File defaultPicture = fileRepository.getDefaultPicture();
            beer.setPicture(defaultPicture);
        }

        return ModelsMapper.getInstance().beerToBeerDTO(beer, averageRating,
                currentUserRating, numberOfRatings, beerTags);
    }

    private List<BeerDTO> mapFilterAndSortBeersToBeerDTOs(List<Beer> beers, Integer userId, String country, String style,
                                                          List<String> tags, Double minAbv, Double maxAbv,
                                                          Double minAvgRating, Double maxAvgRating, String sort, Boolean ascending) {
        List<BeerDTO> beerDTOS = new ArrayList<>();
        for (Beer beer : beers) {
            BeerDTO beerDTO = beerToBeerDTO(beer, userId);
            beerDTOS.add(beerDTO);
        }

        beerDTOS = BeerFilterer.getInstance().filter(beerDTOS, country, style, tags,
                minAbv, maxAbv, minAvgRating, maxAvgRating);
        beerDTOS = BeerSorter.getInstance().sort(beerDTOS, sort, ascending);
        return beerDTOS;
    }

    private Double calculateBeerAverageRating(List<Rating> ratings) {
        double averageRating = 0;
        for (Rating rating : ratings) {
            averageRating += rating.getRating();
        }
        return ratings.size() == 0 ? null : averageRating / ratings.size();
    }
}
