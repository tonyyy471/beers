package com.example.beer.services.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class FieldException extends RuntimeException {
    public FieldException(String message) {
        super(message);
    }
}
