package com.example.beer.services;

import com.example.beer.models.dtos.*;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    UserDTO getUser(HttpServletRequest httpServletRequest);

    void registerUser(CreateUserDTO createUserDTO);

    void updateUser(UpdateUserDTO updateUserDTO, HttpServletRequest httpServletRequest);

    JWTTokenDTO login(LoginDTO loginDTO);
}
