package com.example.beer.services;

import com.example.beer.models.dtos.BeerDTO;
import com.example.beer.models.dtos.CreateBeerDTO;
import com.example.beer.models.dtos.UpdateBeerDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BeerService {
    List<BeerDTO> getAllBeers(HttpServletRequest httpServletRequest, String filterByCountry, String filterByStyle,
                              List<String> filterByTags, Double filterByMinAbv, Double filterByMaxAbv,
                              Double filterByMinAverageRating, Double filterByMaxAverageRating,
                              String sort, Boolean type);

    List<BeerDTO> getCreatedBeers(HttpServletRequest httpServletRequest, String filterByCountry, String filterByStyle,
                                  List<String> filterByTags, Double filterByMinAbv, Double filterByMaxAbv,
                                  Double filterByMinAverageRating, Double filterByMaxAverageRating,
                                  String sort, Boolean type);

    List<BeerDTO> getRatedBeers(HttpServletRequest httpServletRequest, String country, String style,
                                List<String> tags, Double minAbv, Double maxAbv,
                                Double minAvgRating, Double maxAvgRating,
                                String sort, Boolean ascending);

    BeerDTO getBeer(HttpServletRequest httpServletRequest, String beerName);

    void createBeer(HttpServletRequest httpServletRequest, CreateBeerDTO createBeerDTO);

    void updateBeer(HttpServletRequest httpServletRequest, UpdateBeerDTO updateBeerDTO, String beerName);
}
