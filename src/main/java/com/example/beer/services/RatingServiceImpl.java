package com.example.beer.services;

import com.example.beer.models.Beer;
import com.example.beer.models.Rating;
import com.example.beer.models.User;
import com.example.beer.models.dtos.CreateRatingDTO;
import com.example.beer.repostiories.BeerRepository;
import com.example.beer.repostiories.RatingRepository;
import com.example.beer.security.AuthorizedUser;
import com.example.beer.services.validators.BeerValidator;
import com.example.beer.services.validators.RatingValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class RatingServiceImpl implements RatingService {
    private RatingRepository ratingRepository;
    private BeerRepository beerRepository;

    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository, BeerRepository beerRepository) {
        this.ratingRepository = ratingRepository;
        this.beerRepository = beerRepository;
    }

    @Override
    public void rateBeer(HttpServletRequest httpServletRequest, CreateRatingDTO createRatingDTO) {
        User user = AuthorizedUser.getInstance().getUser(httpServletRequest);
        int userId = user.getId();
        String beerName = createRatingDTO.getBeerName();
        double ratingValue = createRatingDTO.getRating();

        Beer beer = beerRepository.getBeer(beerName);
        BeerValidator.getInstance().beerIsNullValidation(beer, false, null);

        RatingValidator.getInstance().ratingIsBetweenOneAndTen(ratingValue);

        Rating rating = ratingRepository.getRating(beer.getId(), userId);

        if (rating != null) {
            rating.setRating(ratingValue);
            ratingRepository.updateRating(rating);
        } else {
            rating = new Rating(ratingValue, userId, beer);
            ratingRepository.createRating(rating);
        }
    }
}
